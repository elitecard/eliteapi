<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonneesGeographiques extends Model
{
    protected $fillable = [
        'longitude',
        'latitude',
        'phase_id',
        'cycle_id',
        'arbre_id',
    ];
}
