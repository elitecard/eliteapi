<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $fillable = [
        'nom',
    ];


    public function plannings()
    {
        return $this->hasMany('App\Planning');
    }
}
