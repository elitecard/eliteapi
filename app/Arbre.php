<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arbre extends Model
{
    protected $fillable = [
        'age',
        'code',
        'cour_taillee',
        'diametre',
        'ecartement',
        'marque',
        'observation',
        'longitude',
        'latitude',
        'origine_semence',
        'rayon',
        'couronne_compact_id',
        'couronne_etalee_id',
        'arbre_ratatine_id',
        'producteur_id',
        'village_id',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function village()
    {
        return $this->belongsTo('App\Village');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producteur()
    {
        return $this->belongsTo('App\Producteur');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function couronne_compact()
    {
        return $this->belongsTo('App\Attribut');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function couronne_etalee()
    {
        return $this->belongsTo('App\Attribut');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function arbre_ratatine()
    {
        return $this->belongsTo('App\Attribut');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function plannings()
    {
        return $this->hasMany('App\Planning');
    }
}
