<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = ['nom'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prefectures()
    {
        return $this->hasMany('App\Prefecture');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producteurs()
    {
        return $this->hasMany('App\Producteur');
    }
}
