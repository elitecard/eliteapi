<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Region;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AgentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = Agent::get();

        return response()->json($agents, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [

            'phone' => 'required|string',
            'code' => 'required|string|unique:agents,code',
            'password' => 'required|string|min:4',
            'region_id' => 'required|integer',

        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $region = Region::find($reformattedRequest->region_id);

        if (!$region) {

            return response()->json(["message " => 'Region not found'], 404);
        }

        $agent = Agent::create([

            'phone' => $reformattedRequest->phone,
            'code' => $reformattedRequest->code,
            'password' => bcrypt($reformattedRequest->password),
            'region_id' => $reformattedRequest->region_id
        ]);


        return response()->json($agent, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent = Agent::find($id);

        if (!$agent) {

            return response()->json(["message " => 'Agent not found'], 404);
        }

        return response()->json($agent, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = Agent::find($id);

        if (!$agent) {

            return response()->json(["message " => 'Agent not found'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [
                'phone' => 'string',
                'code' => 'string|unique:agents,code,' . $agent->id,
                'password' => 'string|min:4',
                'region_id' => 'integer',
            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            if (isset($reformattedRequest->region_id)) {

                $region = Region::find($reformattedRequest->region_id);

                if (!$region) {

                    return response()->json(["message " => 'Region not found'], 404);
                }
            }


            $agent->update([

                'phone' => (isset($reformattedRequest->phone)) ? $reformattedRequest->phone : $agent->phone,
                'code' => (isset($reformattedRequest->code)) ? $reformattedRequest->code : $agent->code,
                'password' => (isset($reformattedRequest->password)) ? bcrypt($reformattedRequest->password) : $agent->password,
                'region_id' => (isset($reformattedRequest->region_id)) ? $reformattedRequest->region_id : $agent->region_id,
            ]);

            return response()->json($agent, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agent = Agent::find($id);

        if (!$agent) {

            return response()->json(["message " => "Agent not found"], 404);
        }

        $result = $agent->delete();

        return response()->json($result, 200);
    }
}
