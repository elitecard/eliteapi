<?php

namespace App\Http\Controllers;

use App\Canton;
use App\Village;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class VillagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $villages = Village::get();

        return response()->json($villages, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'nom' => 'required|string|max:255|unique:villages,nom',
            'canton_id' => 'required|integer'
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $canton = Canton::find($reformattedRequest->canton_id);

        if (!$canton) {

            return response()->json(["message " => "Canton not found"], 404);
        }

        $village = $canton->villages()->create(['nom' => $reformattedRequest->nom]);

        return response()->json($village, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $village = Village::with('canton')->find($id);

        if (!$village) {

            return response()->json(["message " => "Village not found"], 404);
        }

        return response()->json($village, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $village = Village::find($id);

        if (!$village) {

            return response()->json(["message " => 'There is no canton with that name'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [
                'nom' => 'string|max:255|unique:villages,nom,' . $village->id,
                'canton_id' => 'integer'
            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            if(isset($reformattedRequest->canton_id)) {

                $canton = Canton::find($reformattedRequest->canton_id);

                if(!$canton) {

                    return response()->json(["message " => "Prefecture not found"], 404);
                }

            }

            $village->update((array)$reformattedRequest);

            return response()->json($village, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $village = Village::find($id);

        if (!$village) {

            return response()->json(["message " => "Village not found"], 404);
        }

        $result = $village->delete();

        return response()->json($result, 200);
    }
}
