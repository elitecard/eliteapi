<?php

namespace App\Http\Controllers;

use App\Canton;
use App\Prefecture;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class CantonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cantons = Canton::get();

        return response()->json($cantons, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'nom' => 'required|string|max:255|unique:cantons,nom',
            'prefecture_id' => 'required|integer'
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $prefecture = Prefecture::find($reformattedRequest->prefecture_id);

        if (!$prefecture) {

            return response()->json(["message " => "Prefecture not found"], 404);
        }

        $canton = $prefecture->cantons()->create(['nom' => $reformattedRequest->nom]);

        return response()->json($canton, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $canton = Canton::with('prefecture')->find($id);

        if (!$canton) {

            return response()->json(["message " => "Canton not found"], 404);
        }

        return response()->json($canton, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $canton = Canton::find($id);

        if (!$canton) {

            return response()->json(["message " => 'There is no canton with that name'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [
                'nom' => 'string|max:255|unique:cantons,nom,' . $canton->id,
                'prefecture_id' => 'integer'
            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            if(isset($reformattedRequest->prefecture_id)) {

                $prefecture = Prefecture::find($reformattedRequest->prefecture_id);

                if(!$prefecture) {

                    return response()->json(["message " => "Prefecture not found"], 404);
                }

            }

            $canton->update((array)$reformattedRequest);

            return response()->json($canton, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $canton = Canton::find($id);

        if (!$canton) {

            return response()->json(["message " => "Canton not found"], 404);
        }

        $result = $canton->delete();

        return response()->json($result, 200);
    }
}
