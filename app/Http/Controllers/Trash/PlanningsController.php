<?php

namespace App\Http\Controllers;

use App\Planning;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class PlanningsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planning = Planning::get();

        return response()->json($planning, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'type_plan' => 'required|string|max:255',
            'date_debut' => 'required|date',
            'date_fin' => 'required|date',
            'etat' => 'required|boolean',
            'cycle_id' => 'required|integer|exists:cycles,id',
            'phase_id' => 'required|integer|exists:phases,id',
            'arbre_id' => 'required|integer|exists:arbres,id',
            'agent_id' => 'required|integer|exists:agents,id',
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $planning = Planning::create((array)$reformattedRequest);

        return response()->json($planning, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $planning = Planning::with(['cycle', 'agent', 'phase', 'arbre'])->find($id);

        if (!$planning) {

            return response()->json(["message " => 'planning not found'], 404);
        }

        return response()->json($planning, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $planning = Planning::find($id);

        if (!$planning) {

            return response()->json(["message " => 'planning not found'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [

                'type_plan' => 'string|max:255',
                'date_debut' => 'date',
                'date_fin' => 'date',
                'etat' => 'boolean',
                'cycle_id' => 'integer|exists:cycle,id',
                'phase_id' => 'integer|exists:phases,id',
                'arbre_id' => 'integer|exists:arbres,id',
                'agent_id' => 'integer|exists:agents,id',

            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            $planning->update((array)$reformattedRequest);

            return response()->json($planning, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $planning = Planning::find($id);

        if (!$planning) {

            return response()->json(["message " => "planning not found"], 404);
        }

        $result = $planning->delete();

        return response()->json($result, 200);
    }
}
