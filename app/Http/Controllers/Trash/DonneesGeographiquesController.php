<?php

namespace App\Http\Controllers;

use App\DonneesGeographiques;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class DonneesGeographiquesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maps = DonneesGeographiques::get();

        return response()->json($maps, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'longitude' => 'required|numeric|max:255',
            'latitude' => 'required|numeric',
            'cycle_id' => 'required|integer|exists:cycles,id',
            'phase_id' => 'required|integer|exists:phases,id',
            'arbre_id' => 'required|integer|exists:arbres,id',
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $maps = DonneesGeographiques::create((array)$reformattedRequest);

        return response()->json($maps, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $map = DonneesGeographiques::find($id);

        if (!$map) {

            return response()->json(["message " => 'location not found'], 404);
        }

        return response()->json($map, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $map = DonneesGeographiques::find($id);

        if (!$map) {

            return response()->json(["message " => 'location not found'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [

                'longitude' => 'numeric',
                'latitude' => 'numeric',
                'cycle_id' => 'integer|exists:cycles,id',
                'phase_id' => 'integer|exists:phases,id',
                'arbre_id' => 'integer|exists:arbres,id',

            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            $map->update((array)$reformattedRequest);

            return response()->json($map, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $map = DonneesGeographiques::find($id);

        if (!$map) {

            return response()->json(["message " => "location not found"], 404);
        }

        $result = $map->delete();

        return response()->json($result, 200);
    }
}
