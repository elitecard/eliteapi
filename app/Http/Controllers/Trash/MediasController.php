<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class MediasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $media = Media::get();

        return response()->json($media, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'nom' => 'required|string|max:255|unique:media,nom',
            'path' => 'required|string',
            'arbre_id' => 'required|integer|exists:arbres,id',
            'phase_id' => 'required|integer|exists:phases,id',
            'sous_phase_id' => 'required|integer|exists:sous_phases,id'
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $media = Media::create((array)$reformattedRequest);

        return response()->json($media, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $media = Media::find($id);

        if (!$media) {

            return response()->json(["message " => "media not found"], 404);
        }

        return response()->json($media, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $media = Media::find($id);

        if (!$media) {

            return response()->json(["message " => 'media not found'], 404);
        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [
                'nom' => 'string|max:255|unique:media,nom',
                'path' => 'string',
                'arbre_id' => 'integer|exists:arbres,id',
                'phase_id' => 'integer|exists:phases,id',
                'sous_phase_id' => 'integer|exists:sous_phases,id'
            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            $media->update((array)$reformattedRequest);

            return response()->json($media, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::find($id);

        if (!$media) {

            return response()->json(["message " => "media not found"], 404);
        }

        $result = $media->delete();

        return response()->json($result, 200);
    }
}
