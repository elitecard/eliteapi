<?php

namespace App\Http\Controllers;

use App\SousPhase;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class SousPhasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sous_phase = SousPhase::all();

        return response()->json($sous_phase, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'nom' => 'required|string|max:255|unique:sous_phases,nom'
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $sous_phase = SousPhase::create([
            'nom' => $reformattedRequest->nom,
        ]);

        return response()->json($sous_phase, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sous_phase = SousPhase::find($id);

        if (!$sous_phase) {

            return response()->json(["message " => "sous-phase not found"], 404);
        }

        return response()->json($sous_phase, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sous_phase = SousPhase::find($id);

        if (!$sous_phase) {

            return response()->json(["message " => 'sous-phase not found'], 404);
        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [
                'nom' => 'required|string|max:255|unique:sous_phases,nom,' . $sous_phase->id,
            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            $sous_phase->update((array)$reformattedRequest);

            return response()->json($sous_phase, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sous_phase = SousPhase::find($id);

        if (!$sous_phase) {

            return response()->json(["message " => "sous_phase not found"], 404);
        }

        $result = $sous_phase->delete();

        return response()->json($result, 200);
    }
}
