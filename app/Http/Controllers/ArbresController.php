<?php

namespace App\Http\Controllers;

use App\Arbre;
use App\Attribut;
use App\Producteur;
use App\Village;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class ArbresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arbres = Arbre::get();

        return response()->json($arbres, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        //dd($reformattedRequest);

        $validator = Validator::make((array)$reformattedRequest, [

            'age' => 'required|integer',
            'code' => 'required|string|unique:arbres,code',
            'cour_taillee' => 'required|boolean',
            'diametre' => 'required|string',
            'ecartement' => 'required|numeric',
            'marque' => 'required|boolean',
            'observation' => 'required|string',
            'origine_semence' => 'required|string',
            'latitude' => 'required|string',
            'longitude' => 'required|string',
            'rayon' => 'required|string',
            'couronne_compact_id' => 'required|integer|exists:attributs,id',
            'couronne_etalee_id' => 'required|integer|exists:attributs,id',
            'arbre_ratatine_id' => 'required|integer|exists:attributs,id',
            'producteur_id' => 'required|integer|exists:producteurs,id',
            'village_id' => 'required|integer|exists:villages,id',

        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        //dd($request->except(['cour_compact', 'cour_etalee', 'arbre_ratatine', 'producteur_id', 'village_id']));

        $arbre = new Arbre($request->except(['couronnne_compact_id', 'couronne_etalee_id', 'arbre_ratatine_id', 'producteur_id', 'village_id']));

        $couronne_compact = Attribut::find($reformattedRequest->couronne_compact_id);
        $couronne_etalee = Attribut::find($reformattedRequest->couronne_etalee_id);
        $arbre_ratatine = Attribut::find($reformattedRequest->arbre_ratatine_id);
        $producteur = Producteur::find($reformattedRequest->producteur_id);
        $village = Village::find($reformattedRequest->village_id);

        $arbre->couronne_compact()->associate($couronne_compact);
        $arbre->couronne_etalee()->associate($couronne_etalee);
        $arbre->arbre_ratatine()->associate($arbre_ratatine);

        $arbre->producteur()->associate($producteur);
        $arbre->village()->associate($village);

        $arbre->save();

        return response()->json($arbre, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arbre = Arbre::with(['producteur', 'village'])->find($id);

        if (!$arbre) {

            return response()->json(["message " => 'Arbre not found'], 404);
        }

        return response()->json($arbre, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arbre = Arbre::find($id);

        if (!$arbre) {

            return response()->json(["message " => 'Arbre not found'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [

                'age' => 'integer',
                'code' => 'string|unique:arbres,code,' . $arbre->id,
                'cour_taillee' => 'boolean',
                'diametre' => 'string',
                'ecartement' => 'numeric',
                'marque' => 'boolean',
                'observation' => 'string',
                'latitude' => 'required|string',
                'longitude' => 'required|string',
                'origine_semence' => 'string',
                'rayon' => 'string',
                'couronne_compact' => 'integer|exists:attributs,id',
                'couronne_etalee' => 'integer|exists:attributs,id',
                'arbre_ratatine' => 'integer|exists:attributs,id',
                'producteur_id' => 'integer|exists:producteurs,id',
                'village_id' => 'integer|exists:villages,id',

            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            $arbre->update((array)$reformattedRequest);

            return response()->json($arbre, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arbre = Arbre::find($id);

        if (!$arbre) {

            return response()->json(["message " => "Arbre not found"], 404);
        }

        $result = $arbre->delete();

        return response()->json($result, 200);
    }
}
