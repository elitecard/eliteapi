<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Producteur;
use App\Region;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class ProducteursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $producteurs = Producteur::get();

        return response()->json($producteurs, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'code' => 'required|string|max:255|unique:producteurs,code',
            'nom' => 'string',
            'prenoms' => 'string',
            'phone' => 'required|string',
            'region_id' => 'required|integer',
            'agent_id' => 'required|integer',
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }


        $agent = Agent::find($reformattedRequest->agent_id);

        if (!$agent) {

            return response()->json(["message " => "Agent not found"], 404);
        }


        $region = Region::find($reformattedRequest->region_id);

        if (!$region) {

            return response()->json(["message " => "Region not found"], 404);
        }

        $producteur = new Producteur([
            'nom' => (isset($reformattedRequest->nom)) ? $reformattedRequest->nom : '',
            'prenoms' => (isset($reformattedRequest->prenoms)) ? $reformattedRequest->prenoms : '',
            'code' => $reformattedRequest->code,
            'phone' => $reformattedRequest->phone,
        ]);

        $producteur->agent()->associate($agent);

        $producteur->region()->associate($region);

        $producteur->save();

        return response()->json($producteur, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producteur = Producteur::with(['agent', 'region'])->find($id);

        if (!$producteur) {

            return response()->json(["message " => "Producer not found"], 404);
        }

        return response()->json($producteur, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producteur = Producteur::find($id);

        if (!$producteur) {

            return response()->json(["message " => 'Producer not found'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [
                'code' => 'string|max:255|unique:producteurs,code,' . $producteur->id,
                'nom' => 'string',
                'prenoms' => 'string',
                'phone' => 'string',
                'region_id' => 'integer',
                'agent_id' => 'integer',
            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            if (isset($reformattedRequest->agent_id)) {

                $agent = Agent::find($reformattedRequest->agent_id);

                if (!$agent) {

                    return response()->json(["message " => "Agent not found"], 404);
                }

            }

            if (isset($reformattedRequest->region_id)) {

                $region = Region::find($reformattedRequest->region_id);

                if (!$region) {

                    return response()->json(["message " => "Region not found"], 404);
                }

            }

            $producteur->update((array)$reformattedRequest);

            return response()->json($producteur, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producteur = Producteur::find($id);

        if (!$producteur) {

            return response()->json(["message " => "Producer not found"], 404);
        }

        $result = $producteur->delete();

        return response()->json($result, 200);
    }
}
