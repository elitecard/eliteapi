<?php

namespace App\Http\Controllers;

use App\Prefecture;
use App\Region;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class PrefecturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pref = Prefecture::get();

        return response()->json($pref, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'nom' => 'required|string|max:255|unique:prefectures,nom',
            'region_id' => 'required|integer'
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $region = Region::find($reformattedRequest->region_id);

        if (!$region) {

            return response()->json(["message " => "Region not found"], 404);
        }

        $prefecture = $region->prefectures()->create(['nom' => $reformattedRequest->nom]);

        return response()->json($prefecture, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prefecture = Prefecture::with('region')->find($id);

        if (!$prefecture) {

            return response()->json(["message " => "prefecture not found"], 404);
        }

        return response()->json($prefecture, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prefecture = Prefecture::find($id);

        if (!$prefecture) {

            return response()->json(["message " => 'There is no prefecture with that name'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [
                'nom' => 'string|max:255|unique:prefectures,nom,' . $prefecture->id,
                'region_id' => 'integer'
            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            if(isset($reformattedRequest->region_id)) {

                $region = Region::find($reformattedRequest->region_id);

                if(!$region) {

                    return response()->json(["message " => "Region not found"], 404);
                }
            }

            $prefecture->update((array)$reformattedRequest);

            return response()->json($prefecture, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prefecture = Prefecture::find($id);

        if (!$prefecture) {

            return response()->json(["message " => "Prefecture not found"], 404);
        }

        $result = $prefecture->delete();

        return response()->json($result, 200);
    }
}
