<?php

namespace App\Http\Controllers;

use App\Visite;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class VisitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visite = Visite::get();

        return response()->json($visite, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reformattedRequest = json_decode($request->getContent());

        $validator = Validator::make((array)$reformattedRequest, [
            'type_visite' => 'required|string|max:255',
            'observation' => 'required|string',
            'longitude' => 'required|numeric|max:255',
            'latitude' => 'required|numeric|max:255',
            'date_debut' => 'required|date',
            'date_fin' => 'required|date',
            'date_visite' => 'required|date',
            'etat' => 'required|boolean',
            'cycle_id' => 'required|integer|exists:cycles,id',
            'arbre_id' => 'required|integer|exists:arbres,id',
            'agent_id' => 'required|integer|exists:agents,id',
        ]);

        if ($validator->fails()) {

            return response()->json(["message " => $validator->errors()->all()], 400);
        }

        $visite = Visite::create((array)$reformattedRequest);

        return response()->json($visite, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $visite = Visite::with(['cycle', 'agent', 'arbre'])->find($id);

        if (!$visite) {

            return response()->json(["message " => 'visit not found'], 404);
        }

        return response()->json($visite, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $visite = Visite::find($id);

        if (!$visite) {

            return response()->json(["message " => 'visit not found'], 404);

        } else {

            $reformattedRequest = json_decode($request->getContent());

            $validator = Validator::make((array)$reformattedRequest, [

                'type_visite' => 'string|max:255',
                'observation' => 'string',
                'longitude' => 'numeric|max:255',
                'latitude' => 'numeric|max:255',
                'date_debut' => 'date',
                'date_fin' => 'date',
                'date_visite' => 'date',
                'etat' => 'boolean',
                'cycle_id' => 'integer|exists:cycles,id',
                'arbre_id' => 'integer|exists:arbres,id',
                'agent_id' => 'integer|exists:agents,id',

            ]);

            if ($validator->fails()) {

                return response()->json(["message " => $validator->errors()->all()], 400);
            }

            $visite->update((array)$reformattedRequest);

            return response()->json($visite, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visite = Visite::find($id);

        if (!$visite) {

            return response()->json(["message " => "visit not found"], 404);
        }

        $result = $visite->delete();

        return response()->json($result, 200);
    }
}
