<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visite extends Model
{
    protected $fillable = [
        'type_visite',
        'longitude',
        'latitude',
        'date_debut',
        'date_fin',
        'date_visite',
        'etat',
        'observation',
        'cycle_id',
        'arbre_id',
        'agent_id',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cycle()
    {
        return $this->belongsTo('App\Cycle');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function arbre()
    {
        return $this->belongsTo('App\Arbre');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
}
