<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fructification extends Model
{
    protected $fillable = [
        'images_densite',
        'images_maturite',
        'images_fructification',
        'image_melange',
        'densite',
        'est_mature',
        'fructification_homogene',
        'melange',
    ];
}
