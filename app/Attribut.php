<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribut extends Model
{
    protected $fillable = [
        'nom',
    ];
}
