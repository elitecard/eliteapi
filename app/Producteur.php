<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producteur extends Model
{
    protected $fillable = [
        'code',
        'region_id',
        'agent_id',
        'nom',
        'prenoms',
        'phone',
    ];


    public function region()
    {
        return $this->belongsTo('App\Region');
    }


    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
}
