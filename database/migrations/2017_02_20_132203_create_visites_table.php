<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visites', function (Blueprint $table) {

            $table->increments('id');
            $table->string('type_visite');
            $table->string('observation');
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->date('date_debut');
            $table->date('date_fin');
            $table->date('date_visite')->nullable();
            $table->boolean('etat');

            $table->integer('cycle_id')->unsigned()->index();
            $table->foreign('cycle_id')->references('id')->on('cycles');

            $table->integer('arbre_id')->unsigned()->index();
            $table->foreign('arbre_id')->references('id')->on('arbres');

            $table->integer('agent_id')->unsigned()->index();
            $table->foreign('agent_id')->references('id')->on('agents');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visites');
    }
}
