<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArbresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arbres', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('age');
            $table->string('code');
            $table->boolean('cour_taillee');
            $table->string('diametre');
            $table->double('ecartement');
            $table->boolean('marque');
            $table->longText('observation');
            $table->string('origine_semence');
            $table->string('rayon');
            $table->double('longitude');
            $table->double('latitude');

            $table->integer('couronne_compact_id')->unsigned()->index();
            $table->foreign('couronne_compact_id')->references('id')->on('attributs');

            $table->integer('couronne_etalee_id')->unsigned()->index();
            $table->foreign('couronne_etalee_id')->references('id')->on('attributs');

            $table->integer('arbre_ratatine_id')->unsigned()->index();
            $table->foreign('arbre_ratatine_id')->references('id')->on('attributs');

            $table->integer('producteur_id')->unsigned()->index();
            $table->foreign('producteur_id')->references('id')->on('attributs');

            $table->integer('village_id')->unsigned()->index();
            $table->foreign('village_id')->references('id')->on('villages');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arbres');
    }
}
