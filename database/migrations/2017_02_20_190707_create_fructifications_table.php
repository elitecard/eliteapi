<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFructificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     *
     *
    'densite',
    'est_mature',
    'fructification_homogene',
    'melange',
     * @return void
     */
    public function up()
    {
        Schema::create('fructifications', function (Blueprint $table) {
            $table->increments('id');
            $table->text('images_densite');
            $table->text('images_maturite');
            $table->text('images_fructification');
            $table->text('image_melange');
            $table->boolean('est_mature');
            $table->boolean('fructification_homogene');
            $table->boolean('melange');

            $table->integer('densite')->unsigned()->index();
            $table->foreign('densite')->references('id')->on('attributs');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fructifications');
    }
}
