<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plannings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_plan');
            $table->date('date_debut');
            $table->date('date_fin');
            $table->boolean('etat');

            $table->integer('cycle_id')->unsigned()->index();
            $table->foreign('cycle_id')->references('id')->on('cycles');

            $table->integer('phase_id')->unsigned()->index();
            $table->foreign('phase_id')->references('id')->on('phases');

            $table->integer('arbre_id')->unsigned()->index();
            $table->foreign('arbre_id')->references('id')->on('arbres');

            $table->integer('agent_id')->unsigned()->index();
            $table->foreign('agent_id')->references('id')->on('agents');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plannings');
    }
}
