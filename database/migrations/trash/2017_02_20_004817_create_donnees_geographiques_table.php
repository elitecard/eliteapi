<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonneesGeographiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donnees_geographiques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('longitude');
            $table->string('latitude');

            $table->integer('cycle_id')->unsigned()->index();
            $table->foreign('cycle_id')->references('id')->on('cycles');

            $table->integer('phase_id')->unsigned()->index();
            $table->foreign('phase_id')->references('id')->on('phases');

            $table->integer('arbre_id')->unsigned()->index();
            $table->foreign('arbre_id')->references('id')->on('arbres');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donnees_geographiques');
    }
}
